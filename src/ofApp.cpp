#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
    std::string host;

    std::cout << "Please Enter Host IP:" << std::endl;
    std::getline(std::cin, host);
    std::cout << host + " Entered" << std::endl;

    int port = 7400;
    sender.setup(host, port); // 7400 & localhost

    string hostVideo = "http://" + host + ":8080/?action=stream"; // Add stream Identifier Here

    const char* hVideo = hostVideo.c_str();

    ShellExecute(NULL, "open", hVideo,
                NULL, NULL, SW_SHOWNORMAL);
}

//--------------------------------------------------------------
void ofApp::update(){

}

//--------------------------------------------------------------
void ofApp::draw(){

}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){
    // We only Care about KeyPressed
    // Directions
    int FOWARD = 0;
    int BACKWARD = 1;
    int LEFT = 2;
    int RIGHT = 3;
    int STOP = 4;
    // Commands
    int END = 5;
    // Camera
    int UP = 6;
    int DOWN = 7;
    int PANLEFT = 8;
    int PANRIGHT = 9;

    if(key == OF_KEY_UP){
        std::cout << "UP" << std::endl;
        ofxOscMessage m;
        m.setAddress("/direction");
        m.addIntArg(FOWARD);
        sender.sendMessage(m);
    }
    if(key == OF_KEY_DOWN){
        std::cout << "DOWN" << std::endl;
        ofxOscMessage m;
        m.setAddress("/direction");
        m.addIntArg(BACKWARD);
        sender.sendMessage(m);
    }
    if(key == OF_KEY_LEFT){
        std::cout << "LEFT" << std::endl;
        ofxOscMessage m;
        m.setAddress("/direction");
        m.addIntArg(LEFT);
        sender.sendMessage(m);
    }
    if(key == OF_KEY_RIGHT){
        std::cout << "RIGHT" << std::endl;
        ofxOscMessage m;
        m.setAddress("/direction");
        m.addIntArg(RIGHT);
        sender.sendMessage(m);
    }
    if(key == OF_KEY_DEL){
        std::cout << "STOP" << std::endl;
        ofxOscMessage m;
        m.setAddress("/direction");
        m.addIntArg(STOP);
        sender.sendMessage(m);
    }
    if(key == OF_KEY_END){
        std::cout << "END" << std::endl;
        ofxOscMessage m;
        m.setAddress("/command");
        m.addIntArg(END);
        sender.sendMessage(m);
    }
    if(key == 'w'){
        std::cout << "UP CAM" << std::endl;
        ofxOscMessage m;
        m.setAddress("/camera");
        m.addIntArg(UP);
        sender.sendMessage(m);
    }
    if(key == 'a'){
        std::cout << "LEFT CAM" << std::endl;
        ofxOscMessage m;
        m.setAddress("/camera");
        m.addIntArg(PANLEFT);
        sender.sendMessage(m);
    }
    if(key == 's'){
        std::cout << "DOWN CAM" << std::endl;
        ofxOscMessage m;
        m.setAddress("/camera");
        m.addIntArg(DOWN);
        sender.sendMessage(m);
    }
    if(key == 'd'){
        std::cout << "RIGHT CAM" << std::endl;
        ofxOscMessage m;
        m.setAddress("/camera");
        m.addIntArg(PANRIGHT);
        sender.sendMessage(m);
    }
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){

}
